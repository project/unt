<?php
/**
 * @file
 * Admin settings for Unique node title.
 * 
 */

/**
 * Admin form
 */
function unique_node_title_admin_settings() {
  $form = array();

  $form['unique_node_title_settings'] = array(
    '#title' => t('Settings'),
    '#type' => 'fieldset',
  );
  $form['unique_node_title_settings']['unique_node_title_none_global'] = array(
    '#type' => 'checkbox',
    '#title' => t('No global check for node titles.'),
    '#default_value' => variable_get('unique_node_title_none_global'),
    '#description' => t('Check for unique node titles only for node types that have the setting unique node title set to true.'),
  );

  $form = system_settings_form($form);
  return $form;
}
