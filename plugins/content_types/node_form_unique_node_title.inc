<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'icon' => 'icon_node_form.png',
  'title' => t('Set node title as unique'),
  'description' => t('Unique node title form.'),
  'required context' => new ctools_context_required(t('Form'), 'node_form'),
  'category' => t('Form'),
);

function unique_node_title_node_form_unique_node_title_content_type_render($subtype, $conf, $panel_args, &$context) {
  $block = new stdClass();
  $block->module = t('node_form');

  $block->title = t('Unique node title information');
  $block->delta = 'unique-options';

  if (isset($context->form)) {
      $block->content['unique_node_title_setting'] = $context->form['unique_node_title_setting'];
      if (isset($block->content['unique_node_title_setting']['#group'])) {
        unset($block->content['unique_node_title_setting']['#pre_render']);
        unset($block->content['unique_node_title_setting']['#theme_wrappers']);
      }

      // Set access to false on the original rather than removing so that
      // vertical tabs doesn't clone it. I think this is due to references.
      $context->form['unique_node_title_setting']['#access'] = FALSE;
  }
  else {
    $block->content = t('Unique node title checkbox.');
  }
  return $block;
}

function unique_node_title_node_form_unique_node_title_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" checkbox', array('@s' => $context->identifier));
}

function unique_node_title_node_form_unique_node_title_content_type_edit_form($form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
  return $form;
}
